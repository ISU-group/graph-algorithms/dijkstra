```
$ ./build/src/shortest-path ./graph.txt
```

```
1 - 3 - 5 - 9
Length: 21
```

<img src="./graph.png"/>