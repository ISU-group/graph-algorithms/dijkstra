#ifndef __H_DIJKSTRA__
#define __H_DIJKSTRA__

#include <map>
#include <ostream>
#include <string>
#include <vector>

#include <graph.h>

#include "priority_queue.h"

struct Way {
    std::vector<Node*> nodes;
    int length = -1;

    friend std::ostream& operator<<(std::ostream&, const Way&);
};

class Dijkstra {
    const Graph& graph;

    Way unroll(std::map<Node*, WeightedNode> visited, Node*, Node*);

public:
    Dijkstra(const Graph& g);
    Way shortest_way(const std::string&, const std::string&);
};

#endif