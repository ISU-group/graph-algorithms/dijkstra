#ifndef __H_PRIORITY_QUEUE__
#define __H_PRIORITY_QUEUE__

#include <vector>

#include <graph.h>

struct WeightedNode {
    Node *node = nullptr, *prev = nullptr;
    int weight;

    WeightedNode(Node* node_, int weight_, Node* prev_)
        : weight(weight_)
        , node(node_)
        , prev(prev_)
    {
    }
    WeightedNode() = default;
    WeightedNode(const WeightedNode&) = default;

    WeightedNode& operator=(const WeightedNode&) = default;
};

class PriorityQueue {
    std::vector<WeightedNode> nodes;

public:
    WeightedNode pop();
    void push(WeightedNode);
    bool empty() const;
};

#endif