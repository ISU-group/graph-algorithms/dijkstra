#include "priority_queue.h"

WeightedNode PriorityQueue::pop()
{
    WeightedNode node = nodes.back();
    nodes.pop_back();
    return node;
}

void PriorityQueue::push(WeightedNode node)
{
    auto iter = nodes.begin();
    while (iter != nodes.end() && node.weight < iter->weight)
        iter++;

    if (iter == nodes.end())
        nodes.push_back(node);
    else
        nodes.insert(iter, node);
}

bool PriorityQueue::empty() const
{
    return nodes.empty();
}