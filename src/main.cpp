#include <filesystem>
#include <iostream>

#include "graph.h"
#include "shortest_path.h"

int main(int argc, char** argv)
{
    if (argc > 1) {
        auto graph = Graph(argv[1]);
        auto dijkstra = Dijkstra(graph);

        auto way = dijkstra.shortest_way("1", "9");

        // std::cout << graph << std::endl;
        std::cout << way << std::endl;
    }

    return 0;
}