#include "shortest_path.h"

Dijkstra::Dijkstra(const Graph& graph_)
    : graph(graph_)
{
}

Way Dijkstra::shortest_way(const std::string& from, const std::string& to)
{
    PriorityQueue nodes;

    auto origin = graph.find_node(from)->get();
    auto destination = graph.find_node(to)->get();

    if (origin == nullptr || destination == nullptr) {
        throw std::runtime_error("NULL");
    }

    WeightedNode node(origin, 0, nullptr);
    nodes.push(node);

    std::map<Node*, WeightedNode> visited;

    while (!nodes.empty()) {
        auto next = nodes.pop();

        if (visited.find(next.node) != visited.end())
            continue;
        visited[next.node] = next;

        if (destination == next.node) {
            return unroll(visited, origin, destination);
        }

        for (auto neighbor = next.node->cbegin(); neighbor != next.node->cend(); neighbor++) {
            auto weight = neighbor->second + next.weight;
            if (visited.find(neighbor->first) == visited.end()) {
                nodes.push(WeightedNode(neighbor->first, weight, next.node));
            }
        }
    }

    return Way();
}

Way Dijkstra::unroll(std::map<Node*, WeightedNode> visited, Node* from, Node* to)
{
    Way way;
    way.length = visited[to].weight;
    while (to != from) {
        way.nodes.push_back(to);
        to = visited[to].prev;
    }

    way.nodes.push_back(from);
    return way;
}

std::ostream& operator<<(std::ostream& out, const Way& way)
{
    if (!way.nodes.empty()) {
        for (auto i = way.nodes.size() - 1; i > 0; i--) {
            out << way.nodes[i]->get_name() << " - ";
        }
        out << way.nodes.front()->get_name() << std::endl;
        out << "Length: " << way.length << std::endl;
    } else {
        out << "Empty\n";
    }
    return out;
}