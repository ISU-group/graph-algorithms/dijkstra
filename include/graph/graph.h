#ifndef H_GRAPH
#define H_GRAPH

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <set>

#include "node.h"

class Graph {
    using Nodes = std::set<std::unique_ptr<Node>>;

    Nodes nodes;

public:
    explicit Graph(const std::string& path);

    inline void add_node(const std::string&);
    inline void drop_node(const std::string&);

    void add_edge(const std::string&, const std::string&, double = 1);
    void remove_edge(const std::string&, const std::string&);

    bool contains_node(const std::string&) const;
    Nodes::iterator find_node(const std::string&) const;

    friend std::ostream& operator<<(std::ostream&, const Graph&);
};

#endif