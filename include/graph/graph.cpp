#include "graph.h"

Graph::Graph(const std::string& path)
{
    std::ifstream in(path);
    std::string line;

    if (!in.is_open()) {
        throw std::runtime_error("cannot open file");
    }

    // TODO: make buffered reader
    while (std::getline(in, line)) {
        auto start_idx = 0;
        if (auto last_idx = line.find(' '); last_idx != std::string::npos) {
            auto origin = line.substr(start_idx, last_idx);

            add_node(origin);

            start_idx = last_idx + 1;
            last_idx = line.find(' ', start_idx);
            if (last_idx != std::string::npos) {
                auto destination = line.substr(start_idx, last_idx - start_idx);
                add_node(destination);

                start_idx = last_idx + 1;
                auto w = line.substr(start_idx, last_idx - start_idx);
                if (w.empty()) {
                    add_edge(origin, destination);

                } else {
                    add_edge(origin, destination, std::stod(w));
                }
            } else {
                auto destination = line.substr(start_idx);
                if (!destination.empty()) {
                    add_node(destination);
                    add_edge(origin, destination);
                }
            }
        }
    }
}

void Graph::add_node(const std::string& name)
{
    if (!contains_node(name)) {
        nodes.emplace(new Node(name));
    }
}

bool Graph::contains_node(const std::string& name) const
{
    return find_node(name) != nodes.end();
}

Graph::Nodes::iterator Graph::find_node(const std::string& name) const
{
    return std::find_if(nodes.begin(), nodes.end(), [&name](const auto& node) {
        return node->name == name;
    });
}

void Graph::drop_node(const std::string& name)
{
    if (auto node = find_node(name); node != nodes.end()) {
        for (auto& it : nodes) {
            it->neighbors.erase(node->get());
        }

        nodes.erase(node);
    }
}

void Graph::add_edge(
    const std::string& origin,
    const std::string& destination,
    double weight)
{
    auto origin_it = find_node(origin);
    auto destination_it = find_node(destination);

    if (origin_it != nodes.end() || destination_it != nodes.end()) {
        origin_it->get()->neighbors.insert(std::make_pair(destination_it->get(), weight));
    }
}

void Graph::remove_edge(
    const std::string& origin,
    const std::string& destination)
{
    auto origin_it = find_node(origin);
    auto destination_it = find_node(destination);

    if (origin_it != nodes.end() || destination_it != nodes.end()) {
        origin_it->get()->neighbors.erase(destination_it->get());
    }
}

std::ostream& operator<<(std::ostream& out, const Graph& graph)
{
    for (const auto& node : graph.nodes) {
        out << *node;
    }

    return out;
}