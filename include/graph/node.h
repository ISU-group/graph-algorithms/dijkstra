#ifndef H_NODE
#define H_NODE

#include <map>
#include <ostream>
#include <string>

class Node {
    using NodeNeighbors = std::map<Node*, double>;

public:
    using const_iterator = NodeNeighbors::const_iterator;

    explicit Node(const std::string&);

    const_iterator cbegin() const;
    const_iterator cend() const;

    const std::string& get_name() const;

private:
    std::string name;
    NodeNeighbors neighbors;

    friend class Graph;
    friend std::ostream& operator<<(std::ostream&, const Node&);
};

#endif