#include "node.h"

Node::Node(const std::string& name_)
    : name(name_)
{
}

Node::const_iterator Node::cbegin() const
{
    return neighbors.cbegin();
}

Node::const_iterator Node::cend() const
{
    return neighbors.cend();
}

const std::string& Node::get_name() const
{
    return name;
}

std::ostream& operator<<(std::ostream& out, const Node& node)
{
    for (const auto& neighbor : node.neighbors) {
        out << node.name << " - " << neighbor.first->name << ": " << neighbor.second << std::endl;
    }

    return out;
}